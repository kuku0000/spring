package com.example.manqanebi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
@Getter
@Setter
@ToString

public class Cars {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String nomeri;
    private String marka;
    private String tipi;
    private long weli;

    public Cars() {
    }

    public Cars(String nomeri, String marca,String tipi,long weli) {
        this.nomeri = nomeri;
        this.marka = marca;
        this.tipi = tipi;
        this.weli = weli;
    }
}
