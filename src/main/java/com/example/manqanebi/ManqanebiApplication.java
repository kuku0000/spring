package com.example.manqanebi;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class ManqanebiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ManqanebiApplication.class, args);
    }

    @Bean
    public CommandLineRunner run(CarsRepository repository){
        return (args -> {
            insertJavaAdvocates(repository);
            System.out.println(repository.findAll());
            //System.out.println(repository.findCarsByNomeriContaining(""));
        });
    }
    private void insertJavaAdvocates(CarsRepository repository){
        repository.save(new Cars("ku123ku","kukurukuku","rtht",1999));
        repository.save(new Cars("ku235kt","kukurukuku2","etry",2006));
        repository.save(new Cars("ku654kq","kukurukuku3","qoit",2015));
        repository.save(new Cars("ku354rt","kukurukuku4","oertnm",2019));
    }

}
